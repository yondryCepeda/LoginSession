package bean;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpSession;

import entity.Login;
import utils.Util;

@ManagedBean(name = "sessionvalidar")
@SessionScoped
public class SessionValidar {

	
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistence");
	EntityManager em = emf.createEntityManager();
	
	private String usuario;
	private String password;
	List<Login> lista =new ArrayList();
	
	
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String comprobar(String nom, String passw){
		
	
			lista = em.createNamedQuery("Login.ListaUsuarios", Login.class)
					.setParameter("user", usuario)
					.setParameter("password", password)
					.getResultList();
			
		System.out.println("lista de personas"+lista);
		 
		if(lista.isEmpty()) {
			FacesMessage fm = new FacesMessage("user or password wrong","Error msg");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, fm);
			return "inicio.xhtml";
		}
		else {
			return "bienvenido?faces-redirect=true";
		}
		
		
		
		//165732 numero de reporte
		
	}
	
	public String cerrar(){
		HttpSession hs = Util.getSession();
		hs.invalidate();
		return "inicio";
	}
	
}
